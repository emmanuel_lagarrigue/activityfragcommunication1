package com.blacktobacco.activityfragcommunication1;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements OneFragment.OnFragmentInteractionListener{

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Fragment newFragment = OneFragment.newInstance(null, null);
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.add(R.id.theFragment, newFragment).commit();

  }

  @Override public void onLoginAction(String name, String pass) {
    Log.d("Test", "LogIn action name " + name + " pass " + pass);
  }
}
